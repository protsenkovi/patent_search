import pandas as pd
from docx import Document
from time import sleep
import fips
import google_patent
import patent_db_schema

invention_ids_file_name = 'ru_inventions.ids'
utility_models_file_name = 'ru_utility_models.ids'
international_ids_file_name = 'international.ids'
template_file_name = 'shablon.docx'
report_file_name = 'report.docx'
country_codes_file_name = 'country_codes.tsv'
cell_paragraph_style_name = 'ФТД_name'
fips_request_delay_sec = 3 # site refuses to answer if asked faster
google_request_delay_sec = 4

country_codes_df = pd.read_csv(country_codes_file_name, delimiter='\t')
def get_country_name(country_code, country_codes_df):
    return country_codes_df.loc[country_codes_df['alpha2'] == country_code, 'name'].item()

invention_ids = []
try:
    with open(invention_ids_file_name, 'r') as myfile:
        invention_ids = myfile.read().replace(' ','').replace('\r','').split('\n')
        invention_ids = list(filter(lambda x: '' != x and x[0] != '#', invention_ids))
except FileNotFoundError as e:
    print('{} {}'.format(e.strerror, e.filename))

utility_model_ids = []
try:
    with open(utility_models_file_name, 'r') as myfile:
        utility_model_ids = myfile.read().replace(' ','').replace('\r','').split('\n')
        utility_model_ids = list(filter(lambda x: '' != x and x[0] != '#', utility_model_ids))
except FileNotFoundError as e:
    print('{} {}'.format(e.strerror, e.filename))

international_ids = []
try:
    with open(international_ids_file_name, 'r') as myfile:
        international_ids = myfile.read().replace(' ','').replace('\r','').split('\n')
        international_ids = list(filter(lambda x: '' != x and x[0] != '#', international_ids))
except FileNotFoundError as e:
    print('{} {}'.format(e.strerror, e.filename))

try:
    Document(template_file_name).save(report_file_name)
except PermissionError as e:
    print('\nERROR! Can not use the {} document. Please close it or delete it.\n'.format(report_file_name))
    exit(-1)

print('Получаю данные патентов из сети.', end='\n\n')

# Web find section
rows = []
print('Изобретения')
for id in invention_ids:
    print(' ' + id, end='')
    rows.append(fips.find_invention_patent(id))
    print(' ' + 'готов')
    sleep(fips_request_delay_sec)

print('Полезные модели')
for id in utility_model_ids:
    print(' ' + id, end='')
    rows.append(fips.find_utility_model_patent(id))
    print(' ' + 'готов')
    sleep(fips_request_delay_sec)

print('Иностранные патенты')
for id in international_ids:
    print(' ' + id, end='')
    rows.append(google_patent.find_patent(id))
    print(' ' + 'готов')
    sleep(google_request_delay_sec)

# Analysis section
patents_df = pd.DataFrame(rows, columns=patent_db_schema.header)


# Report section
report = Document(template_file_name)
report_table = report.tables[0]

for idx, patent in patents_df.iterrows():
    row = report_table.add_row()
    row.cells[1].add_paragraph(
        '{} ({}), {}, {}.\n {}'.format(
            get_country_name(patent.countryCode, country_codes_df),
            patent.countryCode,
            patent.patentKind,
            patent.id,
            ', '.join(patent.mpk)),
        cell_paragraph_style_name)
    row.cells[2].add_paragraph(
        'Патентообладатель(и): {}.\nЗаявка: {}, {}.\nДата конвенционного приоритета: {}.\nДата публикации: {}'.format(
            ', '.join(patent.patentHolders),
            patent.patentApplicationId,
            patent.patentApplicationDate,
            patent.patentPriorityDate,
            patent.patentPublicationDate),
        cell_paragraph_style_name)
    row.cells[3].add_paragraph(patent.patentName.capitalize(), cell_paragraph_style_name)
    row.cells[4].add_paragraph(patent.status, cell_paragraph_style_name)


report.save(report_file_name)


print('\nРезультаты сохранены в {}.'.format(report_file_name))