from pyquery import PyQuery as pq
import re

def get_patent_id(doc):
    return doc.find('article > dl > meta[itemprop$=numberWithoutCodes]').attr['content']

def get_patent_name(doc):
    return doc.find('section[itemprop*=application] > section[itemprop*=metadata] > span[itemprop$=title]').text()

def get_patent_kind(doc):
    return doc.children().eq(0).text().split(' ')[0][-2:]

def get_patent_country_code(doc):
    return doc.children().eq(0).text().split(' ')[0][:2]

def get_application_id(doc):
    return doc.find('section[itemprop*=application] > section[itemprop*=metadata] > span[itemprop$=applicationNumber]').text()

def get_patent_status(doc):
    patent_status = doc.find('article > dl > dd[itemprop$=legalStatusIfi] > span[itemprop$=status]').text()
    return patent_status if patent_status != '' else 'Application'

def gather_subsequence(doc_children, first_matches, last_matches=''):
    doc_children_len = len(doc_children)
    start = -1
    stop = -1
    for i in range(0, doc_children_len):
        if re.search(first_matches, doc_children.eq(i).text()):
            start = i + 1
            break
    if start == -1:
        return []
    for i in range(0, doc_children_len):
        if re.search(last_matches, doc_children.eq(i).text()):
            stop = i
            break
    if last_matches == '':
        stop = doc_children_len-1
    if stop == -1:
        return []
    result = []
    for i in range(start, stop):
        result.append(doc_children.eq(i).text())
    return result

def get_inventors(doc):
    return [x.text() for x in doc.find('article > dd[itemprop$=inventor]').items()]

def get_current_assignees(doc):
    return [x.text() for x in doc.find('article > dd[itemprop$=assigneeCurrent]').items()]

def get_original_assignees(doc):
    return [x.text() for x in doc.find('article > dd[itemprop$=assigneeOriginal]').items()]

def get_priority_date(doc):
    return doc.find('section[itemprop*=application] > section[itemprop*=metadata] > span[itemprop*=priorityDate]').text()

def get_application_date(doc):
    return doc.find('section[itemprop*=application] > section[itemprop*=metadata] > span[itemprop*=filingDate]').text()

def get_publication_date(doc):
    publication_dates = doc.find('section[itemprop*=application]').eq(-1).find('td[itemprop*=publicationDate]')
    return publication_dates.eq(-1).text()

def get_grant_date(doc):
    return ''.join(gather_subsequence(doc.children(), first_matches='Grant date.*', last_matches='Links.*'))

def get_classifications(doc):
    return [x.text() for x in doc.find('li[itemprop$=cpcs] > meta[itemprop$=Leaf]').parent().find('span[itemprop$=Code]').items()]

base_url = "https://patents.google.com/xhr/result"

def find_patent(id):
    args = {
        'id': 'patent/{}/en'.format(id),
        'qs': 'oq/{}'.format(id),
        'exp':''}
    doc = pq(base_url, args, method='get')
    return [get_patent_id(doc),
            get_patent_country_code(doc),
            get_patent_kind(doc),
            get_application_id(doc),
            get_patent_name(doc),
            get_patent_status(doc),
            get_classifications(doc),
            get_original_assignees(doc),
            get_inventors(doc),
            get_application_date(doc),
            get_publication_date(doc),
            get_grant_date(doc),
            '',
            '',
            '',
            '',
            get_current_assignees(doc),
            get_priority_date(doc)]