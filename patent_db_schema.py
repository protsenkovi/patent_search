header = ['id',
 'countryCode',
 'patentKind',
 'patentApplicationId',
 'patentName',
 'status',
 'mpk',
 'patentHolders',
 'authors',
 'patentApplicationDate',
 'patentPublicationDate',
 'patentCommencementDate',
 'patentPublicationBallot',
 'citationList',
 'correspondenceAddress',
 'patentType',
 'patentHoldersCurrent',
 'patentPriorityDate'
 ]