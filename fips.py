from pyquery import PyQuery as pq
import re

base_url = "http://www1.fips.ru/fips_servl/fips_servlet"

def get_patent_country_code(doc):
    return doc('table').eq(0)('tr').eq(0)('td')('div').eq(1).text()

def get_id(doc):
    return doc('table').eq(0)('tr').eq(0)('td')('div').eq(3).text().replace(' ', '')

def get_patent_kind(doc):
    return doc('table').eq(0)('tr').eq(0)('td')('div').eq(5).text()

def get_mpk(doc):
    return [x.text() for x in doc('table').eq(0)('tr').eq(1).find('span').items()]

def get_status(doc):
    return doc('td#StatusR').text().split('\n')[0]

def get_fee(doc):
    return doc('td#StatusR').text().split('\n')[1]

def get_patent_application_id(doc):
    return doc('table#bib')('tr').eq(0)('td').eq(0)('p').eq(0).find('a').text()

def get_patent_application_date(doc):
    return doc('table#bib')('tr').eq(0)('td').eq(0)('p').eq(0)('b').text().split(',')[1].strip()

def get_patent_commencement_date(doc):
    return doc('table#bib')('tr').eq(0)('td').eq(0)('p').eq(1).find('b').text()

def get_priority_date(doc):
    search_result = re.search(r'\(30\) Конвенционный приоритет:.*?(\d\d\.\d\d\.\d\d\d\d)', doc.find('table#bib').text(), flags=re.RegexFlag.DOTALL)
    if search_result == None:
        return ''
    else:
        return search_result.groups()[0]

def get_patent_publication_date(doc):
    search_p_elements = doc('table#bib')('tr').eq(0)('td').eq(0)('p').items()
    search_result = list(filter(lambda y: '(45) Опубликовано' in y.text(), search_p_elements))
    if len(search_result) != 1:
        return ''
    else:
        value = search_result[0].find('b').eq(0).text().replace('\n','').replace(';','')
        return value

def get_patent_publication_ballot(doc):
    search_p_elements = doc('table#bib')('tr').eq(0)('td').eq(0)('p').items()
    search_result = list(filter(lambda y: '(45) Опубликовано' in y.text(), search_p_elements))
    if len(search_result) != 1:
        return ''
    else:
        value = search_result[0].find('b').eq(1).text().replace('\n', '').replace(';', '')
        return value

def get_citation_list(doc):
    return doc('table#bib')('tr').eq(0)('td').eq(0)('p.B560')('b').text()

def get_rst_phase_date(doc):
    search_p_elements = doc('table#bib')('tr').eq(0)('td').eq(0)('p').items()
    search_result = list(filter(lambda y: '(85) Дата начала рассмотрения заявки PCT на национальной фазе' in y.text(), search_p_elements))
    if len(search_result) != 1:
        return ''
    else:
        value = search_result.find('b').text().replace('\n', '').replace(';', '')
        return value

def get_rst_application(doc):
    search_p_elements = doc('table#bib')('tr').eq(0)('td').eq(0)('p').items()
    search_result = list(filter(lambda y: '(86) Заявка PCT' in y.text(),
                     search_p_elements))
    if len(search_result) != 1:
        return ''
    else:
        value = search_result[0].find('b').text().replace('\n', '').replace(';', '')
        return value

def get_rst_publication_number(doc):
    search_p_elements = doc('table#bib')('tr').eq(0)('td').eq(0)('p').items()
    search_result = list(filter(lambda y: '(87) Публикация заявки PCT' in y.text(),
                     search_p_elements))
    if len(search_result) != 1:
        return ''
    else:
        value = search_result[0].find('b')('pnum').text().replace('\n', '').replace(';', '')
        return value

def get_rst_publication_date(doc):
    search_p_elements = doc('table#bib')('tr').eq(0)('td').eq(0)('p').items()
    search_result = list(filter(lambda y: '(87) Публикация заявки PCT' in y.text(),
                     search_p_elements))
    if len(search_result) != 1:
        return ''
    else:
        value = search_result[0].find('b').remove('pnum').text().replace('(', '').replace(')', '')
        return value

def get_correspondence_address(doc):
    search_p_elements = doc('table#bib')('tr').eq(0)('td').eq(0)('p').items()
    search_result = list(filter(lambda y: 'Адрес для переписки' in y.text(),
                     search_p_elements))
    if len(search_result) != 1:
        return ''
    else:
        value = search_result[0].find('b').text().replace('(', '').replace(')', '')
        return value

def get_authors(doc):
    return doc.find('td#bibl')('p').eq(0)('b').text().replace('\n', '').split(',')

def get_patent_holders(doc):
    return doc.find('td#bibl')('p').eq(1)('b').text().replace('\n', '').split(',')

def get_patent_name(doc):
    return doc('p#B542').find('b').text()

databases = {
    'invention': 'RUPAT',
    'utility_model' : 'RUPM'
}

def find_patent(id, patent_type):
    args = {'DB': databases[patent_type],
            'DocNumber':id}
    doc = pq(base_url, args, method="post")
    if doc('table#bib')('tr').eq(0)('td').eq(0).has_class('B002'):
        doc('table#bib')('tr').eq(0).remove()
    try:
        return [get_id(doc),
                get_patent_country_code(doc),
                get_patent_kind(doc),
                get_patent_application_id(doc),
                get_patent_name(doc),
                get_status(doc),
                get_mpk(doc),
                get_patent_holders(doc),
                get_authors(doc),
                get_patent_application_date(doc),
                get_patent_publication_date(doc),
                get_patent_commencement_date(doc),
                get_patent_publication_ballot(doc),
                get_citation_list(doc),
                get_correspondence_address(doc),
                patent_type,
                '',
                get_priority_date(doc)]
    except IndexError as e:
        print(id, e)

def find_invention_patent(id):
    return find_patent(id, 'invention')

def find_utility_model_patent(id):
    return find_patent(id, 'utility_model')